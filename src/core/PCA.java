package core;
import java.util.Arrays;
import java.util.Comparator;
import Jama.EigenvalueDecomposition;
import Jama.Matrix;
/**
 * The class that implements the PCA algorithm
 * This class uses the JAMA library for Matrix operations:
 * addition, multiplication, eigenvalue decomposition
 * and print operations.
 * To use this class first create an instance of PCA with the 
 * raw data as an argument (as JAMA Matrix), then call runPCA 
 * method with the desired number of components. 
 * NOTE: runPCA MUST be called before calling showFirstNResults
 * method.
 * 
 * @author ana_makarevich
 * Please cite properly when using this code:
 * cite the author's name and a link to this repository
 */
public class PCA {
	private Matrix sourceData;
	private int n;
	private int d;
	private Matrix normalizedData;
	private Matrix covMatrix;
	private EigenvalueDecomposition eigons;
	private Matrix evectors;
	private Integer[] indices;
	private Matrix resultMatrix;
	
	/**
	 * Constructor for the PCA object
	 * Takes as input the source data in a Matrix form
	 * @param sourceData - raw unnormalized data in a 
	 * from a 
	 */
	public PCA(Matrix sourceData) {
		this.sourceData = sourceData;
		this.n = sourceData.getRowDimension();
		this.d = sourceData.getColumnDimension();
		// normalize
		normalize();
		//build the covariance matrix
		buildCovMatrix();
		// Get eigenvalues, eigenvectors and sorted array of indices
		getEigenvalues();
	}
	
	/**
	 * Calculates eigenvalues, eigenvector, 
	 * then sorts eigenvalues and saves the indices order
	 * as an array of doubles
	 */
	private void getEigenvalues() {
		eigons = new EigenvalueDecomposition(covMatrix);
		double[] eigenvals = getEigenVals(eigons.getD());
		indices = getIndices(eigenvals);
		evectors = eigons.getV();
	}
	
	/**
	 * Runs the dimension reduction operation for
	 * the given number of principal components
	 * @param n_components  number of principal components to use
	 * @return result matrix with the reduced number of columns
	 */
	public Matrix runPCA(int n_components) {
		resultMatrix = new Matrix(n, n_components);
		// Loop over rows
		double midRes = 0;
		for (int i = 0; i < n; i++) {
			// for ever row we loop over each eigenvector 
			for (int k = 0; k < n_components; k++) {
				// pick up the index of the next principal component
				int comp_index = indices[k];
				for(int j = 0; j < d; j++ ) {
					midRes+=sourceData.get(i, j)*evectors.get(j, comp_index);
				}
				resultMatrix.set(i, k, midRes);
				midRes = 0;
			}
		}
		return resultMatrix;
	}

	/**
	 * Sorts an array in the descending order
	 * @param eigenvals - array to be sorted
	 * @return array of indices in the order corresponding
	 * to the eigenvals sorted in the descending order
	 */
	private Integer[] getIndices(double[] eigenvals) {
		ArrayIndexComparator comparator = new ArrayIndexComparator(eigenvals);
		Integer[] indexes = comparator.createIndexArray();
		Arrays.sort(indexes, comparator);
		return indexes;	
	}
	
	/**
	 * Transforms diagonal matrix into an array
	 * @param D block diagonal eigenvalue matrix
	 * @return array of eigenvalues
	 */
	
	private double[] getEigenVals(Matrix D) {
		int size = D.getRowDimension();
		double [] evals = new double[size];
		for (int i = 0; i < size; i++) {
			evals[i] = D.get(i, i);
		}
		return evals;
	}
	
	/**
	 * Builds the covariance matrix for the source data
	 * The resulting matrix is d x d
	 */
	private void buildCovMatrix() {
		int n_cols = normalizedData.getColumnDimension();
		int n_rows = normalizedData.getRowDimension();
		covMatrix = new Matrix(n_cols, n_cols);
		double [] [] source = normalizedData.getArray();
		for (int i = 0; i < n_rows; i++){
			covMatrix.plusEquals(outerProduct(source[i]));
		}
		covMatrix.timesEquals(1.0/n_rows);
	}
	
	/**
	 * Calculates the means for each dimension
	 * for the original matrix
	 * @return the 1xd matrix of means 
	 */
	private Matrix mean() {
		int n_cols = sourceData.getColumnDimension();
		Matrix result = new Matrix(1,n_cols);
		int n_rows = sourceData.getRowDimension();
		for (int i = 0; i < n_rows; i++) {
			for (int j = 0; j < n_cols; j++) {
				result.set(0, j, result.get(0, j)+sourceData.get(i, j));
			}
		}
		result.timesEquals(1.0/n_rows);
		return result;
	}
	
	/**
	 * Normalizes the data by substracting the mean from 
	 * each data point
	 * @return normalized dataset
	 */
	private void normalize() {
		Matrix mean = mean();
		normalizedData = new Matrix(n, d);
		for(int i = 0; i < n; i++) {
			for (int j=0; j < d; j++) {
				normalizedData.set(i, j, sourceData.get(i, j)-mean.get(0, j));
			}
		}	
	}
	
	/** 
	 * Calculates the outer product of a vector with itself: (X^T)*T
	 * 
	 * @param point a data point as an array of doubles of size n
	 * @return n x n matrix
	 */
	private static Matrix outerProduct(double[] point) {
		int dim = point.length;
		Matrix result = new Matrix(dim,dim);
		for (int i = 0; i < dim; i++){
			for (int j = 0; j < dim; j++) {
				result.set(i, j, point[i]*point[j]);
			}
		}
		return result;
		
	}
	/**
	 * Prints out eigenvalues as
	 * a block matrix with eigenvalues on the
	 * diagonal
	 */
	public void showEigenvalues() {
		eigons.getD().print(6, 4);
	}
	/**
	 * Prints out the the first n observations
	 * (rows) of the raw data
	 * @param z number of rows to print
	 */
	public void showFirstNSource(int z){
		(sourceData.getMatrix(0, z-1, 0, d-1)).print(6,4);
	}
	
	/**
	 * Prints out the the first n observations
	 * (rows) of the resulting matrix
	 * @param z number of rows to print
	 */
	public void showFirstNResult(int z) {
		int resDim = resultMatrix.getColumnDimension();
		(resultMatrix.getMatrix(0, z-1, 0, resDim-1)).print(6,4);
	}
	
	/**
	 * Prints out the the first n observations
	 * (rows) of the normalized data
	 * @param z number of rows to print
	 */
	public void showFirstNNorm(int z) {
		(normalizedData.getMatrix(0, z-1, 0, d-1)).print(6,4);
	}
	/**
	 * Prints out the covariance matrix
	 */
	public void showCovMatrix(){
		covMatrix.print(6, 4);
	}
	/**
	 * Prints out the eigenvectors,
	 * where eigenvectors are columns in
	 * the output matrix
	 */
	public void showEigenVectors(){
		evectors.print(6, 4);
	}
}

/**
 * The following class was borrowed from
 * StackOverflow with minor changes to make
 * the code suitable for this case. This function is 
 * necessary to get the ordering of the eigenvalues.
 * 
 */
class ArrayIndexComparator implements Comparator<Integer>
{
    private final double[] array;

    public ArrayIndexComparator(double[] array)
    {
        this.array = array;
    }

    public Integer[] createIndexArray()
    {
        Integer[] indexes = new Integer[array.length];
        for (int i = 0; i < array.length; i++)
        {
            indexes[i] = i; // Autoboxing
        }
        return indexes;
    }

    @Override
    public int compare(Integer index1, Integer index2)
    {
         // Autounbox from Integer to int to use as array indexes
        return (int) (array[index2] - array[index1]);
    }
}